#
# Be sure to run `pod lib lint Analytics.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Analytics'
  s.version          = '0.1.5'
  s.summary          = 'Analytics for Adbox projects'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = 'Wrapper for Firebase Analytics or Google Analytics.'

  s.homepage         = 'https://ishuster23@bitbucket.org/ishuster23'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Aleks C. Barragan' => 'leks.bar@icloud.com' }
  s.source           = { :git => 'https://ishuster23@bitbucket.org/ishuster23/analyticswrapper.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '11.0'
  s.swift_version = '4.0'
  s.source_files = 'Analytics/Classes/*.swift'
  s.source_files = 'Analytics/*'
  s.source_files = 'Classes/*.swift'
  
  # s.resource_bundles = {
  #   'Analytics' => ['Analytics/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
